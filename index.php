<!DOCTYPE html>
<html lang="en">

<head>

    <?php include('config.php');?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png" href="favicon.png">

    <title>Mucky Pups - Home</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/ncreative.css" rel="stylesheet">
    <link href="css/jcreative.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

    <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1197269833694227";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll animated fadeIn" href="index.php">Home</a>
            </div>

            <!-- Collect the nav links, forms, and other content for togglinwg -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll animated fadeIn" href="#prices">Prices</a>
                    </li>
                    <li>
                        <a class="page-scroll animated fadeIn" href="#services">Services</a>
                    </li>
                    <li>
                        <a class="page-scroll animated fadeIn" href="#gallery">Gallery</a>
                    </li>
                    <li>
                        <a class="page-scroll animated fadeIn" href="booking_form.php">Book Now</a>
                    </li>
                    <li>
                        <a class="page-scroll animated fadeIn" href="#find">Find Us</a>
                    </li>
                    <li>
                        <a class="page-scroll animated fadeIn" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <img class="logo" id="logo-position" src="img/logo2.png" alt="" width="407" height="314">
                <h1 id="homeHeading" class="animated pulse">Mucky Pups</h1>
                <hr>
                <p>Welcome to Mucky Pups!</p>
                <p>Located in Dronfield, we are your local dog grooming choice!</p>
                <a href="#services" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
            </div>
        </div>
    </header>

    <aside class="bg-dark" id="comp">
      <form action="index.php#comp" method="post">
        <div class="form-login" style="width: 50%; margin: auto; box-shadow: 0px 0px 20px 2px #8a8a8a">
          <div class="squishy">
            <h3 style="text-align: center; color: black; width: 100%">Competition</h3><br>
            <img src="img/comp.png" alt="Comp" style="max-width:300px; width:100%; display:block; margin:auto;"><br><br>
          </div>
          <p style="color: black; text-align: center">Up for grabs is a full set of shampooing products made by V endurance</p>
          <p style="color: black; text-align: center">Please enter your details below to enter!</p>
          <p style="font-size: 10px;color: black; text-align: center">One entry per household only, multiple enteries will be denied.</p>
          <input style="text-align: center" name="first" type="text" class="form-control input-sm chat-input" placeholder="First Name" />
          <input style="text-align: center" name="last" type="text" class="form-control input-sm chat-input" placeholder="Last Name" />
          <input style="text-align: center" name="email" type="email" class="form-control input-sm chat-input" placeholder="Enter your email here" />
          <br>
          <button type="submit" name="compbtn" style="color: white; background: rgba(230, 126, 229, 0.47); width: 100%" class="btn btn-lg">Submit</button>

          <?php

          if(isset($_POST['compbtn'])) {

            $first = $_POST['first'];
            $last = $_POST['last'];
            $email = $_POST['email'];

            $sql = "INSERT INTO comp (first, last, email) VALUES ('$first', '$last', '$email')";
            if (mysqli_query($connect, $sql)) {

            } else {
              echo "this is an error" . $connect->error;
            }
          }

          ?>

        </div>
      </form>
    </aside>

    <section class="bg-primary" id="prices">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Online Booking &amp Price Calculator</h2>
                    <hr class="light">

                    <br>
                    <p class="text-faded">Get your estimated price below!</p>

                    <form id="calc" class="box box1 shadow1" action="index.php#prices" method="post">

                      <h3 class="section-heading" style="color: black">Please select your dog size below</h3>
                      <select name="size" class="btn-calc pricecheckbox">
                        <option name="size" value="small">Small</option>
                        <option name="size" value="medium">Medium</option>
                        <option name="size" value="large">Large</option>
                      </select>
                      <br>
                      <br>
                      <br>
                      <h3 class="section-heading" style="color: black">Would you like any extra's?</h3>
                      <div class="checkboxstyle">
                      <input type="checkbox" name="bath" value="bath"> A quick bath? <br>
                      <input type="checkbox" name="nail" value="nail"> Nail clipping? <br>
                      </div>
                      <br>
                      <br>

                      <input style="font-size: 20px" type="submit" name="pricebtn" class="btn btn-default btn-xl sr-button" value="Get your price!">
                      <br>
                      <br>

                      <?php

                      if(isset($_POST['pricebtn'])) {

                        $size = $_POST['size'];
                        $sql1 = "SELECT price FROM price WHERE service = '$size'";
                        $priceresult1 = mysqli_query($connect, $sql1);
                        $pricesize=mysqli_fetch_array($priceresult1,MYSQLI_ASSOC);


                        if (!isset($_POST['bath'])) {
                          $pricebath = '0';
                          $bathchk = '0';
                        } else {
                          $sql2 = "SELECT price FROM price WHERE service = 'bath'";
                          $priceresult2 = mysqli_query($connect, $sql2);
                          $pricebath = mysqli_fetch_array($priceresult2,MYSQLI_ASSOC);
                          $pricebath = $pricebath['price'];
                          $bathchk = '1';
                        }


                        if (!isset($_POST['nail'])) {
                          $pricenail = '0';
                          $nailchk = '0';
                        } else {
                          $sql3 = "SELECT price FROM price WHERE service = 'nail'";
                          $priceresult3 = mysqli_query($connect, $sql3);
                          $pricenail = mysqli_fetch_array($priceresult3,MYSQLI_ASSOC);
                          $pricenail = $pricenail['price'];
                          $nailchk = '1';
                        }


                      $totalprice = $pricesize['price'] + $pricebath + $pricenail;
                      $totalextras = $bathchk + $nailchk;

                      echo "<div class=\"pricecalc animated fadeInDown fadeIn\">Only £$totalprice!</div>";
                      echo "<div class=\"pricecalc2 animated fadeInLeft fadeIn\">For a $size dog cut,</div>";
                      echo "<div class=\"pricecalc3 animated fadeInRight fadeIn\">With $totalextras extra(s).</div>";
                      }

                      ?>

                      <br>
                      <hr class="light">
                      <a href="booking_form.php" style="font-size: 30px" class="page-scroll btn btn-default btn-xl sr-button">Book Now!</a>

                    </form>




                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">At Your Service</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <img src="img/bone.png">
                        <h3>CARE</h3>
                        <p class="text-muted">Here at Mucky Pups we care about your pet as if they were our own!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <img src="img/comb.png">
                        <h3>GROOMING</h3>
                        <p class="text-muted">Want your pet to be groomed in a certain way? no problem!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <img src="img/scissors.png">
                        <h3>CLIPPING</h3>
                        <p class="text-muted">With over 10 years in the industry, you can trust in Muck Pups!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <img src="img/shampoo.png">
                        <h3>BATHING</h3>
                        <p class="text-muted">Don't worry about bathing your pet before coming, we are here for that as well!</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="no-padding" id="gallery">
        <div class="container-fluid">
            <div class="row no-gutter popup-gallery">
                <div class="col-lg-4 col-sm-6">
                    <a href="img/portfolio/fullsize/1.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/1.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                              <div class="project-category text-faded">
                                <h3>Testimonal</h3>
                              </div>
                              <div class="project-name">
                                <p>"Our two smelt lovely after coming home from here."</p>
                                  <h5>Posted by Tina Paynter Thursday, July 23, 2015</h5>
                              </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/portfolio/fullsize/2.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/2.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                              <div class="project-category text-faded">
                                <h3>Testimonal</h3>
                              </div>
                              <div class="project-name">
                                <p>"Brilliant service would recommend to anyone 1st class."</p>
                                  <h5>Posted by Chris Rawson Wednesday, April 26, 2017</h5>
                              </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/portfolio/fullsize/3.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/3.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                              <div class="project-category text-faded">
                                <h3>Testimonal</h3>
                              </div>
                              <div class="project-name">
                                <p>"Nice lady who is very calm and relaxed with dogs.
                                  Did a lovely job and good price.
                                  We'll be back soon"</p>
                                  <h5>Posted by Julie Briggs Wednesday, February 15, 2017</h5>
                              </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/portfolio/fullsize/4.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/4.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                  <h3>Testimonal</h3>
                                </div>
                                <div class="project-name">
                                  <p>"I have no problems at all with leaving Stanley here. He must enjoy it as he
                                     pulls me into the doorway when we walk past. He has been about half a dozen
                                     times for a full groom and always comes out looking lovely, unlike when he
                                     goes in! He is quite a stubborn boy and wont do anything he doesn't like so
                                     if he willingly pulls me through the door and looks comfortable wandering
                                     around the shop then I'm happy. Thoroughly recommended."</p>
                                    <h5>Posted by Lesley Austen Friday, Novemeber 28, 2014</h5>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/portfolio/fullsize/5.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/5.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                              <div class="project-category text-faded">
                                <h3>Testimonal</h3>
                              </div>
                              <div class="project-name">
                                <p>"Thank you for another super clean up and cut for Rosie.... and for
                                  putting up with her 'dad', lol . She looks and smells gorgeous x"</p>
                                  <h5>Posted by June Butterworth Thursday, May 5, 2016</h5>
                              </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/portfolio/fullsize/6.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/6.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                              <div class="project-category text-faded">
                                <h3>Testimonal</h3>
                              </div>
                              <div class="project-name">
                                <p>"i take my bella she allways comes out smelling nice and
                                  very well cut i recomend mucky pups :)"</p>
                                  <h5>Posted by Liam Karl Shelton Thursday, October 03, 2013</h5>
                              </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-primary" id="news">
      <form action="index.php#news" method="post">
        <div class="form-login" style="width: 50%; margin: auto; box-shadow: 0px 0px 20px 2px #8a8a8a">
          <h4 style="font-size: 25px; color: black">Newsletter</h4>
          <p style="color: black; text-align: center">Subscribe to our newsletter to recieve news and offers!</p>
          <input style="text-align: center" name="newsemail" type="email" class="form-control input-sm chat-input" placeholder="Enter your email here" />
          <br>
          <button type="submit" name="newsbtn" style="color: white; background: rgba(230, 126, 229, 0.47); width: 100%" class="btn btn-lg">Submit</button>

          <?php

          if(isset($_POST['newsbtn'])) {

            $email = $_POST['newsemail'];

            $sql = "INSERT INTO newsletter (email) VALUES ('$email')";
            if (mysqli_query($connect, $sql)) {

            } else {
              echo "this is an error" . $connect->error;
            }
          }

          ?>

        </div>
      </form>
    </section>

    <aside class="bg-dark" id="find">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>Find Us!</h2>
                <iframe width="70%" height="450" frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/place?q=mucky%20pups%20dronfield&key=AIzaSyARL2gbjGUE4HPKJTsfgpVaCqf9PF_YEJ8" allowfullscreen></iframe>
            </div>
        </div>
    </aside>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Get in Touch!</h2>
                    <hr class="primary">
                    <p>Either fill out our contact form, Give us a call or Send us an email!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>07429 378 112</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:gemma@muckypups.xyz">gemma@muckypups.xyz</a></p>
                </div>

                <div class="col-sm-6 col-sm-offset-3">
                    <div class="well" style="margin-top: 10%;">
                    <h2>Send us a message</h2>
                    <form role="form" id="contactForm" data-toggle="validator" class="shake">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="name" class="h4">Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter name" required data-error="Required">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="email" class="h4">Email</label>
                                <input type="email" class="form-control" id="email" placeholder="Enter email" required data-error="Email not valid.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="h4 ">Message</label>
                            <textarea id="message" class="form-control" rows="5" placeholder="Enter your message" required data-error="Required"></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                        <button type="submit" id="form-submit" class="btn btn-success btn-lg pull-right ">Submit</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                        <div class="clearfix"></div>
                    </form>
                    </div>
                </div>
            </div>
</section>

</section>
</div>
    <div class="bg-primary">
        </br>
        <H4>NJ Development</H4>
        </br>
    </div>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/creative.min.js"></script>
    <script src="js/ncreative.min.js"></script>
    <script src="js/jcreative.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="js/validator.min.js"></script>
    <script type="text/javascript" src="js/form-scripts.js"></script>

</body>

</html>
