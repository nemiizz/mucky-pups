<!DOCTYPE html>
<html lang="en">

<head>

    <?php include('config.php');?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mucky Pups - Booking Form</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/ncreative.css" rel="stylesheet">
    <link href="css/jcreative.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

     <link rel='stylesheet' href='plugins/fullcalendar/fullcalendar.css' />
     <link rel='stylesheet' href='plugins/fullcalendar/fullcalendar.print.css' />
     <link rel='stylesheet' href='plugins/timepicker/jquery.timepicker.css' />
     <link rel='stylesheet' href='css/jquery-ui.min.css' />
     <link rel="icon" type="image/png" href="favicon.png">

</head>

<body id="page-top">
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php">Home</a>
            </div>

            <!-- Collect the nav links, forms, and other content for togglinwg -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                  <li>
                      <a class="page-scroll animated fadeIn" href="index.php#prices">Prices</a>
                  </li>
                  <li>
                      <a class="page-scroll animated fadeIn" href="index.php#services">Services</a>
                  </li>
                  <li>
                      <a class="page-scroll animated fadeIn" href="index.php#gallery">Gallery</a>
                  </li>
                  <li>
                      <a class="page-scroll animated fadeIn" href="#form">Book Now</a>
                  </li>
                  <li>
                      <a class="page-scroll animated fadeIn" href="index.php#find">Find Us</a>
                  </li>
                  <li>
                      <a class="page-scroll animated fadeIn" href="index.php#contact">Contact</a>
                  </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                 <div class="row">

                        <div class = "form-container">

                        <?php
                             $lable = "Continue";
                             $button = 'booking-page1-submit';
                             $button2 = 'booking-page2-submit';
                             if(isset($_POST[$button2])) {
                                  booking_submit();
                             }
                             elseif( ! isset($_POST[$button])) { ?>
                                  <form class="booking-form" id="form" action="booking_form.php#form" method="post">
                                       <fieldset>
                                            <legend class="legend">Booking Form</legend>
                                            <hr>
                                            Title* :
                                            <select class="select" name="title" size="1">
                                                 <option name="title" value="Mr">Mr</option>
                                                 <option name="title" value="Ms">Ms</option>
                                                 <option name="title" value="Mrs">Mrs</option>
                                                 <option name="title" value="Miss">Miss</option>
                                            </select> <br> <br>
                                            First Name* :
                                            <input type="text" name="firstname" placeholder="First Name"> <br> <br> <br>
                                            Last Name* :
                                            <input type="text" name="lastname" placeholder="Last Name"> <br> <br> <br>
                                            Email* :
                                            <input type="text" name="email" placeholder="Your Email Address"> <br> <br> <br>
                                            Phone Number* :
                                            <input type="text" name="mobile" maxlength="11" placeholder="Phone Number"> <hr>
                                        </fieldset>
                                       <input class="page-scroll animated zoomIn" type="submit" <?php echo "name='$button' value='$lable'"?>>
                             <?php
                             }
                             elseif (isset($_POST[$button]))
                             {

                                  $button2 = 'booking-page2-submit';
                                  $lable = 'Submit';
                                  customer_validation();
                                  ?>
                                  <form class="booking-form" id="form" action="booking.php" method="post">
                                       <input type="hidden" name="title" value="<?php echo $_POST['title']; ?>">
                                       <input type="hidden" name="firstname" value="<?php echo $_POST['firstname']; ?>">
                                       <input type="hidden" name="lastname" value="<?php echo $_POST['lastname']; ?>">
                                       <input type="hidden" name="email" value="<?php echo $_POST['email']; ?>">
                                       <input type="hidden" maxlength="11" name="mobile" value="<?php echo $_POST['mobile']; ?>">
                                        <fieldset>
                                             <legend class="legend">Booking Details:</legend>
                                             <hr>
                                             Please choose a date for the appointment:
                                             <input type="text" name="date" class="picker" id="datepicker">  <br> <br>
                                             <div class="time-picker">
                                                  Please choose a time for the appointment: <i class="fa fa-clock-o"></i> <input type="text" name="time" class="picker" placeholder="&#128337;" id="time" value="--">  <br> <br>
                                            </div>
                                            How many pets do you require to be groomed? <br>
                                            <select class="select" name="num_pets" size="1">
                                            <?php
                                                 $number = 6;
                                                 for ($i=1; $i < $number; $i++) {
                                                      echo "<option name='num_pets' value='$i'>$i</option>";
                                                 }
                                            ?>

                                            </select> <br> <br>
                                             <b> Would you like any extras? </b> <br>
                                             <input type="checkbox" name="bath" value="1">Bathing <br>
                                             <input type="checkbox" name="nail" value="1">Nails
                                             <hr>
                                        </fieldset>
                                        <input class="page-scroll animated zoomIn" type="submit" <?php echo "name='$button2' value='$lable'"?>>
                                        <?php

                                  }
                                        ?>
                                  </form>
                              </div>
                            </div>
                    </div>
               </div>
            </div>
        </div>
    </header>

    <?php







    ?>

    <section class="contact-booking">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Get in Touch!</h2>
                    <hr class="primary">
                    <p>Either fill out our contact form, Give us a call or Send us an email!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>07429 378 112</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:gemma@muckypups.xyz">gemma@muckypups.xyz</a></p>
                </div>
            </div>
        </div>
    </section>

    <div class="bg-primary">
        <br>
        <H4>NJ Development</H4>
        <br>
    </div>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/creative.min.js"></script>


    <script src='plugins/fullcalendar/lib/moment.min.js'></script>
    <script src='plugins/fullcalendar/lib/jquery.min.js'></script>
    <script src='plugins/fullcalendar/fullcalendar.js'></script>
    <script type='text/javascript' src='plugins/fullcalendar/gcal.js'></script>
    <script type='text/javascript' src='plugins/timepicker/jquery.timepicker.min.js'></script>
    <script type='text/javascript' src='js/jquery-ui.min.js'></script>
    <script type='text/javascript' src='js/script.js'></script>
    <script type='text/javascript' src='js/ncreative.js'></script>
    <script type='text/javascript' src='js/jcreative.js'></script>
</body>

</html>
