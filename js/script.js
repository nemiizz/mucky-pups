
// script for JqueryUI date picker
$(function() {
    $( "#datepicker" ).datepicker({
      showOn: "both",
      buttonText: "<i class='fa fa-calendar'></i>",
      beforeShowDay: $.datepicker.noWeekends,
      dateFormat: 'dd-mm-yy'
    });
  });
// script for JqueryUI time picker
/*$('#time').timepicker({
    'minTime': '9:00am',
    'maxTime': '5:00pm',
    'showDuration': false,
    'useSelect': true
});*/

$('#time').timepicker({
    'minTime': '9:00am',
    'maxTime': '3:00pm',
    'showDuration': false,
    'useSelect': true,
    'step': 120
});

// script for fullcalendar
$(document).ready(function() {
$('#calendar').fullCalendar({
  // put your options and callbacks here
  googleCalendarApiKey: 'AIzaSyB9l5DMm_uagHT5JdZAhcYsds79v73AxWc',
  events: {
       googleCalendarId: '12767laff0lqe4jl7en9rcgqm0@group.calendar.google.com'
  }
     });
});
