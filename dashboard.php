<!DOCTYPE html>
<html lang="en">

<head>

    <?php include('config.php');?>


    <?php
    if (!$userID) {
         header ('location: index.php');
   };

    ?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png" href="favicon.png">

    <title>Mucky Pups - Admin Panel</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/ncreative.css" rel="stylesheet">
    <link href="css/jcreative.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div style="background-color: rgba(34, 34, 34, 0.62)" class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php">Back to Website</a>
            </div>

            <!-- Collect the nav links, forms, and other content for togglinwg -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="signout.php">Logout</a>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>

      <div class="header-content" style="top: 20%;">
        <div class="header-content-inner">

          <img class="logo" id="logo-position" src="img/logo2.png" alt="" width="407" height="314">
          <h1 id="homeHeading">Admin Panel</h1><br><br>

        </div>
      </div>

        <div class="admin-content">
            <div class="adminbox1">
              <table class="confirmed-bookings">
                   <h2 class="animated flipInX" style="margin-bottom: 30px">Confirmed Bookings</h2>
                <tr>
                  <th class="admin-header">Name</th>
                  <th class="admin-header">Date</th>
                  <th class="admin-header">Time</th>
                  <th class="admin-header">Phone Number</th>
                  <th class="admin-header">Number of Pets</th>
                </tr>

                  <?php
                  $booking_id = isset($_GET['id']);
                  if($booking_id) {
                       $booking_id = $_GET['id'];
                       $sql = "DELETE FROM `booking` WHERE `booking`.`booking_id` = $booking_id" ;
                       if(mysqli_query($connect, $sql)) {
                       }
                       else{
                            echo("Error description: " . mysqli_error($connect));
                       }
                  }
                  $sql = "SELECT * FROM booking WHERE confirmed = 1 ORDER BY booking_id ";
                  $sql_customer = "SELECT * FROM customer ORDER BY customer_id";
                  $query = mysqli_query($connect, $sql);
                  $query_customer = mysqli_query($connect,$sql_customer);
                  $rows = $query->num_rows;
                  $rows_customer = $query_customer->num_rows;
                  for ($i=0; $i < $rows; $i++) {
                       $result = mysqli_fetch_array($query,MYSQLI_BOTH);
                       $result_customer = mysqli_fetch_array($query_customer,MYSQLI_BOTH);
                       $name = $result_customer['forename'] . " " . $result_customer['surname'];
                       $date = date("d/m/Y", strtotime($result['date_time']));
                       $time = date("H:i:s",strtotime($result['date_time']));
                       $phone = $result_customer['phone_number'];
                       $number_pets = $result['num_pets'];
                       $id = $result['booking_id'];
                       ?>
                                        <tr class="admin-content">
                              <?php echo    "<td> $name </td>
                                             <td> $date </td>
                                             <td>$time</td>
                                             <td>$phone</td>
                                             <td>$number_pets</td>
                                             <td><a href='dashboard.php?id=$id'> Delete </a></td>" ;?>
                                        </tr>
                                   <?php
                  }
                  ?>
              </table>
            </div>

<?php


?>

<div class="adminbox2">
     <h2 class="animated flipInX" style="margin-bottom: 30px">Competition Entries</h2>
 <table class="confirmed-bookings">
    <tr>
      <th class="admin-header">Name</th>
      <th class="admin-header">Email</th>
    </tr>

      <?php
      $sql2 = "SELECT * FROM comp ORDER BY first ";
      $query = mysqli_query($connect, $sql2);
      $rows2 = $query->num_rows;
      for ($i=0; $i < $rows2; $i++) {
          $result = mysqli_fetch_array($query,MYSQLI_BOTH);
          $name = $result['first'] . " " . $result['last'];
          $email = $result['email'];
          ?>
                            <tr class="admin-content">
                  <?php echo     "<td> $name </td>
                                 <td>$email</td>" ;?>
                            </tr>
                       <?php
      }
      ?>
 </table>
</div>

<div class="adminbox3">
     <h2 class="animated flipInX" style="margin-bottom: 30px">Newsletter</h2>
 <table class="confirmed-bookings">
    <tr>
      <th class="admin-header">Email</th>
    </tr>

      <?php
      $sql = "SELECT * FROM newsletter ORDER BY email ";
      $query = mysqli_query($connect, $sql);
      $rows = $query->num_rows;
      for ($i=0; $i < $rows; $i++) {
          $result = mysqli_fetch_array($query,MYSQLI_BOTH);
          $email = $result['email'];
          ?>
                            <tr class="admin-content">
                  <?php echo     "<td>$email</td>" ;?>
                            </tr>
                       <?php
      }
      ?>
 </table>
</div>
    </header>

    <div class="bg-primary">
        </br>
        <H4>NJ Development</H4>
        </br>
    </div>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/creative.min.js"></script>
    <script src="js/ncreative.min.js"></script>
    <script src="js/jcreative.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="js/validator.min.js"></script>
    <script type="text/javascript" src="js/form-scripts.js"></script>

</body>

</html>
